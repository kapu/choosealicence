---
title: Comment publier ma thèse
layout: default
permalink: /comment-these/
---

Hey
<p>
  Les licences  <a href="licenses/cc-by-4.0">creative commons</a> sont un ensemble de licences faciles d'utilisation et modulable selon vos envies.
</p>

# Je veux publier ma thèse sous licence creative commons

  <img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> [CC-BY 4.0](../licenses/cc-by-4.0)

  <img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /> [CC-BY-SA 4.0](../licenses/cc-by-sa-4.0)

  <img alt="Licence Creative Commons" style="border-width:0" src="https://upload.wikimedia.org/wikipedia/commons/f/f9/CC-Zero-badge.svg" /> [CC-0](../licenses/cc0-1.0)
