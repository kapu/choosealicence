#ToDo:

 * Supprimer mes codes pour intégrer des vidéos (objets et le reste, cf partie en cours du README)
 * Pour chaque grosse section, créer une page type non-licence pour expliquer en bref ce qui existe (TRES synthètique) avec plusieurs liens:
  * Vers licences directement
  * vers explications de licences :vidéos, contexte, etc.
   * résumés
   * détaillés
 * Pour les pages type licence: utiliser la fonction de copier au presse-papier pour les images de CC par exemple, ou texte évetuel. PAs la licence en entier. Laisser les licences en entier pour ceux qui voudraient aller plus loin. Pas la priorité à traduire.
  * modif les logos pour les sections.



## Essais d'intégrations de vidéos

Intégrations de vidéos explicatives quand disponibles (notamment pour les CC, éventuellement vidéo de ben goldacre?, domaine public, etc.)
Pour ça:
 * ajout d'un objet? dans meta.yml:
 ex:
```bash
   - name: video
   plateforme: plateforme
   description: Eventuelle vidéo explicative
   required: false
```
 * Puis dans la fiche de licence, appel de cet objet:
 ```bash
 ex: video.plateforme: vimeo
 video: 95488932
```
 * Enfin, intégration pour le moment dans la sidebar.html pour les tests d'appel:
 ```bash
 ex: {% if page.video %}
   {% if page.video.plateforme == youtube %}

   <div class="video">
     <iframe class="youtube-player" type="text/html" width="100%" height="auto" src="https://www.youtube.com/embed/{{ page.video }}" allowfullscreen frameborder="0">
   </iframe>
   </div>

   {% elsif page.video.plateforme == vimeo %}
   <div class="video">
     <iframe class="vimeo-player" width="100%" height="auto"  src="https://player.vimeo.com/video/{{{ page.video }}" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
   </div>

   {% endif %}


 {% endif %}

 ```

 **Problème rencontré pour le moment: choix automatique tu lecteur à charger puis problème de lecture de vidéo vimeo**

## Todo list:

 * collecter les données sur les licences
  * etalab
  * CC
  * C0
  * ?
 * Synthétiser les utilisations des licences en haut de page: lecture <2 min avec clés d'utilisations. Puis licence détaillée en dessous.
 * Trouver un moyen de publier la page depuis github ou gitlab
directement.


## Lancez ça sur votre machine

```bash
git clone https://kapu@framagit.org/kapu/choosealicence.git
cd choosealicense
script/bootstrap
script/server
```

## Notes de travail

 * Pour ajouter des tags aux licences:
  * si c'est un nouveau tag, l'ajouter dans le fichier _data/rules.yml
sur le même modèle que les autres
  * puis l'ajouter dans le ficihier .txt de la licence dans _licenses

 * Pour ajouter de nouvelles licences: fichiers .txt dans le dossier
_licences
 * penser à supprimer les liens avec google analytics

### Propositions de travail

#### Page d'accueil

Revoir le code couleur? (me paraît bien)

Revoir les logo:
 * plus proche de la santé ou des études.
 * modifier les accolades pour autre chose.
Logo de LHC?

Modifier le footer (penser aussi à about et ToS)

Bien accréditet Github et respect des licences!

Proposer éventuellement de contacter LHC pour de l'aide/des conseils.


#### Les creative commons

Faire une description générale des licences avec le texte de la cc-by
éventuellement.
Mais surtout rediriger vers le choose licence du site des CC
[par ici](https://creativecommons.org/choose/?lang=fr).

Mettre une vidéo sur les CC?

# Choosealicense.com

[![Build Status](https://travis-ci.org/github/choosealicense.com.png?branch=gh-pages)](https://travis-ci.org/github/choosealicense.com)

Like a Choose Your Own Adventure site, but only much less interesting.

## Intro

A lot of repositories on GitHub.com don't have a license. GitHub provides a license chooser, but if you don't know anything about licenses, how are you supposed to make an informed decision?

[ChooseALicense.com](http://www.choosealicense.com "Choose A Licence website") is designed to help people make an informed decision about licenses by demystifying license choices through non-judgmental guidance.

## Immediate Goals

* Non-judgmental. Our goal is to help you find a license that meets *your* goals.
* Well designed, but that goes without saying.
* The homepage should have just enough to help 99% of folks make a decision.
* For the 1%, the site will contain a list of licenses common to specific communities and situations.
* Not comprehensive. Seems like an odd goal, but there are a bajillion licenses out there. We're going to have to filter that down to a small list of those that matter.

## Run It On Your Machine

```bash
git clone https://github.com/github/choosealicense.com.git
cd choosealicense.com
script/bootstrap
script/server
```

Open `http://localhost:4000` in your favorite browser.

## Adding a license

For information on adding a license, see [the CONTRIBUTING file](https://github.com/github/choosealicense.com/blob/gh-pages/CONTRIBUTING.md#adding-a-license).

## License metadata

Licenses sit in the `/_licenses` folder. Each license has YAML front matter describing the license's properties. The body of the file contains the text of the license in plain text. The available metadata fields are:

#### Required fields

* `title` - The license full name specified by http://spdx.org/licenses/
* `spdx-id` - Short identifier specified by http://spdx.org/licenses/
* `source` - The URL to the license source text
* `description` - A human-readable description of the license
* `how` - Instructions on how to implement the license
* `permissions` - Bulleted list of permission rules
* `conditions` - Bulleted list of condition rules
* `limitations` - Bulleted list of limitation rules

#### Optional fields

* `featured` - Whether the license should be featured on the main page (defaults to false)
* `hidden` - Whether the license is neither [popular](https://opensource.org/licenses) nor fills out the [spectrum of licenses](http://choosealicense.com/licenses/) from strongly conditional to unconditional (defaults to true)
* `nickname` - Customary short name if applicable (e.g, GPLv3)
* `note` - Additional information about the licenses
* `using` - A list of up to 3 notable projects using the license with straightforward LICENSE files which serve as examples newcomers can follow and that can be detected by [licensee](https://github.com/benbalter/licensee) in the form of `project_name: license_file_url`
* `redirect_from` - Relative path(s) to redirect to the license from, to prevent breaking old URLs

### Auto-populated fields

The licenses on choosealicense.com are regularly imported to GitHub.com to be used as the list of licenses available when creating a repository. When we create a repository, we will replace certain strings in the license with variables from the repository. These can be used to create accurate copyright notices. The available variables are:

#### Fields

* `fullname` - The full name or username of the repository owner
* `login` - The repository owner's username
* `email` - The repository owner's primary email address
* `project` - The repository name
* `description` - The description of the repository
* `year` - The current year

## License properties

The license properties (rules) are stored as a bulleted list within the licenses YAML front matter. Each rule has a name e.g., `include-copyright`, a human-readable label, e.g., `Copyright inclusion`, and a description `Include the original copyright with the code`. To add a new rule, simply add it to `_data/rules.yml` and reference it in the appropriate license.

### Rules

#### Permissions

* `commercial-use` - This software and derivatives may be used for commercial purposes.
* `modifications` - This software may be modified.
* `distribution` - This software may be distributed.
* `private-use` - This software may be used and modified in private.
* `patent-use` - This license provides an express grant of patent rights from contributors.

#### Conditions

* `include-copyright` - A copy of the license and copyright notice must be included with the software.
* `document-changes` - Changes made to the code must be documented.
* `disclose-source` - Source code must be made available when the software is distributed.
* `network-use-disclose` - Users who interact with the software via network are given the right to receive a copy of the source code.
* `same-license` - Modifications must be released under the same license when distributing the software. In some cases a similar or related license may be used.
* `same-license--file` - Modifications of existing files must be released under the same license when distributing the software. In some cases a similar or related license may be used.
* `same-license--library` - Modifications must be released under the same license when distributing the software. In some cases a similar or related license may be used, or this condition may not apply to works that use the software as a library.

#### Limitations

* `trademark-use` - This license explicitly states that it does NOT grant trademark rights, even though licenses without such a statement probably do not grant any implicit trademark rights.
* `liability` - This license includes a limitation of liability.
* `patent-use` - This license explicitly states that it does NOT grant any rights in the patents of contributors.
* `warranty` - The license explicitly states that it does NOT provide any warranty.

## License

The content of this project itself is licensed under the [Creative Commons Attribution 3.0 license](http://creativecommons.org/licenses/by/3.0/us/deed.en_US), and the underlying source code used to format and display that content is licensed under the [MIT license](http://opensource.org/licenses/mit-license.php).
