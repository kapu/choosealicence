---
title: Je veux publier ma thèse
layout: default
permalink: /ma-these/
---

## Je veux mieux comprendre les options possibles en quelques minutes

**<span style="color:#5f99b1">Que se passe-t-il si je ne précise rien dans ma thèse?</span>**

Si vous ne précisez rien au sein de votre thèse, elle est alors soumise au droit d'auteur.

**<span style="color:#5f99b1">Si je met mon travail sous licence libre, il ne sera alors plus protégé?</span>**

Tout dépend de ce que vous voulez.

Vous souhaitez par exemple autoriser le partage de votre travail ainsi que ses modifications, il suffit alors d'utiliser la licence [CC-BY](../licenses/cc-by-4.0). Toute personne ré-utilisant votre travail devra respecter ces conditions et vous citer. C'est une obligation légale, au même titre que le droit d'auteur.
Si vous souhaitez déposer votre travail dans le domaine public, toute personne pourra alors reprendre votre travail, le modifier, le diffuser sans restriction.

### <span style="color:#149AD4">Les licences ouvertes</span>

#### <span style="color:#5f99b1">Les creative commons en France en 2min</span>

<div class="video" align="center">
  <iframe class="vimeo-player" type="text/html" width="auto" height="auto" src="https://player.vimeo.com/video/95488932" allowfullscreen frameborder="0">
</iframe>
</div>


**En résumé:** les creative commons sont des licences ouvertes reconnues en France protégeant les oeuvres et leurs auteurs. Elles facilitent le partage et différents niveaux de libertés peuvent être accordées.

#### <span style="color:#5f99b1">Qu'est-ce que cela implique pour mon travail?</span>

***Je souhaite utiliser en parti le travail d'une autre personne sous licence creative commons***

Il suffit de regarder quelle licence est utilisée et de la respecter pour pouvoir ré-utiliser l'oeuvre en question.
Par exemple, je souhaite ré-utiliser un graphique mis sous licence CC-BY-SA 4.0. En l'incluant dans mon travail je devrais alors: citer l'auteur et mettre ce graphique sous le même type de licence. Cela est possible même si le reste de mon travail est sous une autre licence.

***Je souhaite faire publier mon travail***

En indiquant dans votre travail qu'il est placé sous Creative Commons, il est alors protégé et leur publication devra respecter la licence utilisée.

***Je souhaite partager le travail d'un-e autre***

C'est toujours aussi simple si la personne a publiée son travail sous licence ouverte. Il suffit de lire le pictogramme ou le nom de licence et d'en respecter les conditions pour pouvoir la partager.



### <span style="color:#149AD4">Le droit d'auteur</span>

#### <span style="color:#5f99b1">Le droit d'auteur en France en 2min37</span>

<div class="video" align="center">
  <iframe class="youtube-player" type="text/html" width="auto" height="auto" src="https://www.youtube.com/embed/GezEgVsdW-8" allowfullscreen frameborder="0">
</iframe>
</div>

**En résumé:** via les droits patrimoniaux et moraux, le détenteur du droit d'auteur ont le contrôle sur la reproduction, la distribution et l'affichage de leurs oeuvres. Ils contrôlent aussi les versions dérivées de leurs oeuvres, comme les traduction par exemple.

#### <span style="color:#5f99b1">Qu'est-ce que cela implique pour mon travail?</span>

***Je souhaite utiliser en parti le travail soumis au droit d'auteur d'une autre personne***
Il peut arriver que vous souhaitiez reprendre le travail de quelqu'un pour aller plus loin encore, ou le traduire par exemple.
Si ce travail est soumis au droit d'auteur, en dehors de brèves citations, vous devez demander explicitement à l'auteur l'autorisation de reprendre ou proposer une autre version de son travail.

***Je souhaite faire publier ma thèse***
Selon le moyen de publication il est possible que vous soyez amenés à céder vos droits d'auteur à l'organisme qui publiera votre travail. Bien que vous en serez encore l'auteur vous devrez faire les même demande que n'importe qui pour pouvoir diffuser, modifier, améliorer votre travail.

***Je souhaite partager le travail d'un-e autre***

De la même manière que précédemment vous devez avoir l'accord explicite du détenteut du droit d'auteur pour cela. Cela peut-être accordé gratuitement ou contre rémunération.

***Combien de temps mon travail est-il protégé par le droit d'auteur?***

Il est protégé jusqu'à 70 ans après le décès de l'auteur en France, même si plusieurs exceptions existent. Suite à cette période l'oeuvre passe alors dans le domaine public.

Mais cette notion peut être amenée à changer, cette durée était de 50 ans avant 1993

### <span style="color:#149AD4">Le domaine public</span>

Vous pouvez décider de déposer votre travail directement dans le domaine public. Il s'agit du cas de figure le plus simple, tout le monde pourra partager, modifier votre oeuvre sans restriction.

## Je veux aller plus loin

* [Droit d'auteur sur wikipedia](https://fr.wikipedia.org/wiki/Droit_d%27auteur)
* [Droit d'auteur sur droits-publics](https://www.service-public.fr/professionnels-entreprises/vosdroits/F23431)
* [Vidéo sur le droit d'auteur par DanyCaligula](https://www.youtube.com/watch?v=EQbs9WM-3Q4)
* [Droit d'auteur suite à la publication d'une thèse par l'université de Bordeaux](http://bibliotheques.u-bordeaux.fr/Vos-services/Theses-et-memoires-numeriques/These-de-doctorat/Droits-d-auteur)
* [Domaine public en France sur Wikipedia](https://fr.wikipedia.org/wiki/Domaine_public_en_droit_de_la_propri%C3%A9t%C3%A9_intellectuelle_fran%C3%A7ais)
